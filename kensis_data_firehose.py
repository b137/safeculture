import logging
from data_consumer import DataConsumer
from serverless_read import ServerlessRead
class KensisDataFireHose:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    def __init__(self):
        self.sr = ServerlessRead()
        self.consumer = DataConsumer()

    def set_stream_data(self, stream_data):
        if(not stream_data.empty):
            api_data, db_data = self.trigger_lambda_functions()
            self.save_to_live_data_consumer(stream_data, "stream")
            self.save_to_live_data_consumer(api_data, "api_data")
            self.save_to_live_data_consumer(db_data, "db_data")

            self.save_to_aggregation_data_consumer(stream_data, "stream")
            self.save_to_aggregation_data_consumer(api_data, "api_data")
            self.save_to_aggregation_data_consumer(db_data, "db_data")

    def trigger_lambda_functions(self):
        '''
        trigger lambda to get api and database data when receive live data
        '''
        self.logger.info("Trying to connect to API and database for reference data")
        api_data=self.sr.get_data_from_api()
        db_data=self.sr.get_data_from_db()
        return api_data, db_data
    
    def save_to_live_data_consumer(self, data, table):
        '''
        save data to database for customer to use
        '''
        self.consumer.save_data_to_redshift(data, table)

    def save_to_aggregation_data_consumer(self, data, table):
        '''
        save data to s3 for spark to pick up the app
        '''
        self.consumer.save_data_to_s3(data, table)