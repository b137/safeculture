import logging

class DataConsumer:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    def __init__(self):
        pass

    def save_data_to_redshift(self, data, table):
        '''
        save data to database
        '''
        data.to_csv("redshift/{}_redshift_data.csv".format(table), index=False)
        self.logger.info("{} has been inserted into database".format(table))

    def save_data_to_s3(self, data, table):
        '''
        save data to s3
        '''
        data.to_csv("s3/{}_s3_data.csv".format(table), index=False)
        self.logger.info("{} has been saved in s3".format(table))