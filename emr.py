import logging
import pandas as pd
import numpy as np
import configparser
class EMR:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    def __init__(self):
        # read config for agg columns
        config = configparser.ConfigParser()
        config.read("config.ini")
        self.columns = map(str.strip, config.get("columns", "columns").split(','))

    def data_transformation(self):
        '''
        join datasets between api, database and live data
        '''
        api_data, db_data, stream_data = self.read_data_from_s3()
        reference_data_df = pd.merge(api_data, db_data, on="email", how="outer")
        full_data_df = pd.merge(stream_data, reference_data_df, on="user_id", how="outer").dropna()
        full_data_df["item_value"] = pd.to_numeric(full_data_df["item_value"])
        self.save_data_to_db(full_data_df, "full_data")
        self.data_aggregation(full_data_df)

    def save_data_to_db(self, data, table):
        '''
        save data to database
        '''
        data.to_csv("redshift/{}.csv".format(table), index=False)

    def read_data_from_s3(self):
        '''
        read data from s3
        '''
        api_data = pd.read_csv("s3/api_data_s3_data.csv")
        db_data = pd.read_csv("s3/db_data_s3_data.csv")
        stream_data = pd.read_csv("s3/stream_s3_data.csv")
        return api_data, db_data, stream_data

    def data_aggregation(self, data):
        '''
        aggregation data
        '''
        columns = ["region", "company_name", "industry", "name"]
        export_df = data.groupby(columns)["item_value"].agg(
            total_value=np.sum, min_value=np.min, max_value=np.max, mean_value=np.mean, median_value=np.median, count=np.size
        ).reset_index()
        self.save_data_to_db(export_df, "_".join(columns))
