import pandas as pd
import numpy as np
import json
import os.path
import logging
from kensis_data_firehose import KensisDataFireHose

class DataProducer:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    def __init__(self):
        self.kdf = KensisDataFireHose()

    def get_stream_data(self):
        '''
        get live data from s3
        '''
        with open("sample_data/message_bus.json") as f:
            live_data = json.load(f)
        return live_data

    def check_file_exists(self):
        '''
        check any source file in s3
        if yes then start processing
        '''
        if(os.path.isfile("sample_data/message_bus.json")):
            self.logger.info("Found new stream data in s3")
            live_data = self.get_stream_data()
            self.push_data_to_kensis(self.data_formate_change(live_data))
            return True
        return False

    def push_data_to_kensis(self, data):
        '''
        push data to kensis 
        '''
        self.logger.info("Data pushed to kensis")
        self.kdf.set_stream_data(data)

    def data_formate_change(self, live_data):
        '''
        change source data format
        '''
        rows = []
        for data in live_data:
            items = data.get("items")
            user_id = data.get("user_id")
            checklist_id = data.get("checklist_id")
            checklist_name = data.get("checklist_name")
            for item in items:
                item["user_id"] = user_id
                item["checklist_id"] = checklist_id
                item["checklist_name"] = checklist_name
                rows.append(item)
        return pd.DataFrame(rows)

