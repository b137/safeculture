import pandas as pd
import numpy as np
import json
import logging
from data_producer import DataProducer
from emr import EMR

class Application:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    def __init__(self):
        self.dp = DataProducer()

    def app_start(self):
        started = self.dp.check_file_exists()
        self.logger.info("The status of app start: {}".format(started))

if __name__ == "__main__":
    # assumed there is an airflow job trigger application
    app = Application()
    app.app_start()
    # assumed there is an airflow job called emr
    emr = EMR()
    emr.data_transformation()