import logging
import pandas as pd
import numpy as np
class ServerlessRead:
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    def __init__(self):
        pass

    def get_data_from_api(self):
        '''
        get source data from api
        '''
        self.logger.info("Trying to connect to API")
        api_data = pd.read_json("sample_data/3rd_party_data.json").rename(columns={"user_email":"email"})
        return api_data

    def get_data_from_db(self):
        '''
        get source data from database
        '''
        self.logger.info("Trying to connect to database for reference data")
        db_data = pd.read_csv("sample_data/database.csv")
        return db_data